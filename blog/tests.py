from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test import Client
from django.urls import reverse

from .models import Post


class BlogTests(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testname',
            email='test@mail.com',
            password='secret5612'
        )

        self.post = Post.objects.create(
            title='post_name',
            author=self.user,
            body='test_post_body'
        )

        self.client = Client()

    def test_string_representation(self):
        self.assertEqual(str(self.post), self.post.title)

    def test_post_content(self):
        self.assertEqual(self.post.title, 'post_name')
        self.assertEqual(self.post.author.username, 'testname')
        self.assertEqual(self.post.body, 'test_post_body')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'post_name')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/1000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'post_name')
        self.assertTemplateUsed(response, 'post.html')

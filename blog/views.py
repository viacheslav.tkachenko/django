from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from .models import Post


class HomePageView(ListView):
    model = Post
    template_name = 'home.html'


class PostPageView(DetailView):
    model = Post
    template_name = 'post.html'


class CreatePostView(CreateView):
    model = Post
    template_name = 'create_post.html'
    fields = '__all__'


class UpdatePostView(UpdateView):
    model = Post
    fields = ['title', 'body']
    template_name = 'update_post.html'

